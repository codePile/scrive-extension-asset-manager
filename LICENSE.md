## License
-------
codePile PBC - Web publishing framework and security validation

Copyright 2014-2015 by the contributors

Original source by Assarte D'Raven (webapper.hu) https://github.com/assarte/TwigAssets

-----

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see http://opensource.org/licenses/GPL-3.0.

This program(s) incorporates work covered by copyright and permission notices:

### Wherever third party code has been used, credit has and will be given in the code's comments.

> codePile PBC - Web publishing framework and security validation

> Copyright 2014-2015 by the contributors

> TwigAssets is released under the GPLv3


-----------------------------------------------------------------------------------------------